'use strict';

require('now-logs')('notifyre');
var express = require('express');
var app = express();
var http = require('http');
var bodyParser = require('body-parser');
var bunyan = require('bunyan');
var config = require('config');
var router = require('./router');
var path = require('path');
var logger = bunyan.createLogger({
  name: 'server'
});

app.use(bodyParser.urlencoded({
    extended: true 
}));
app.use(bodyParser.json());
app.use('/', router);

app.use('/static', express.static(path.join(__dirname, 'assets')))

var server = http.createServer(app);
server.listen(config.get('app.port'), function () {
    logger.info({port: config.get('app.port')}, 'Server started');
});
