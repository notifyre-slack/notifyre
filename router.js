'use strict';

var express = require('express');
var config = require('config');
var controllers = require('./lib/controllers');
var firebase = require('./lib/firebase-controller');
var router = express.Router();
var join = require('path').join;

router.get('/healthcheck', function (req, res) {
  res.status(200).send(config.get('app.name') + ' server is healthy.');
});
router.post('/userId', controllers.postUserId);
router.post('/delete', controllers.postDeleteUser);
router.post('/update', controllers.postUpdateUser);
router.post('/notify', controllers.postNotification);
router.get('/auth', firebase.getTokenFromSlackAndPersist);

// home page
router.get('/', function (req, res) {
  return res.sendFile(join(__dirname, './assets/bot-raven.html'));
});
router.get('/terms-of-service', function (req, res) {
  return res.sendFile(join(__dirname, './assets/tos.html'));
});

module.exports = router;
