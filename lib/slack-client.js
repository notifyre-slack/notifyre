'use strict';

var config = require('config');
var request = require('request');

var RtmClient = require('@slack/client').RtmClient;
var RTM_EVENTS = require('@slack/client').RTM_EVENTS;
var RTM_CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS.RTM;
var MemoryDataStore = require('@slack/client').MemoryDataStore;
var CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;

var bunyan = require('bunyan');
var logger = bunyan.createLogger({
  name: 'lib/slack-client',
  serializers: bunyan.stdSerializers,
  src: true
});

var slackBaseRequest = {
  baseUrl: config.get('slack.baseUrl'),
  json: true,
  timeout: 10000,
  useQuerystring: true
};

function SlackClient() {
  this.slackId = config.get('slack.client.id');
  this.slackSecret = config.get('slack.client.secret');
  this.request = request.defaults(slackBaseRequest);
  return this;
};

SlackClient.prototype.initRTMLChannel = function (token, cb) {
  var self = this;
  this.channel = new RtmClient(token, {
    logLevel: 'error',
    // Initialise a data store for our client, 
    // this will load additional helper functions for the storing and retrieval of data
    dataStore: new MemoryDataStore()
  });
  this.channel.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, function (x) {
    logger.info('RTM Channel opened Successfully');
    return cb(null);
  });
  this.channel.on('error', function (err) {
    logger.error({ err: err }, 'RTMP Channel opening error');
    return cb(err);
  });
  this.channel.on('close', function () {
    logger.info('Received close event');
  });
  this.channel.on('goodbye', function () {
    logger.info('Received goodbye event');
  });
  this.channel.start();
}

SlackClient.prototype.openChannelForUser = function (token, userId, done) {
  var self = this;
  logger.info({
    token: token
  }, 'Starting to open a channel');
  var options = {
    uri: '/im.open',
    qs: {
      token: token,
      user: userId
    }
  };
  self.request.get(options, function (err, res, body) {
    if (err) {
      logger.error({err: err}, 'Error opening channel for user');
      return done(err);
    }
    if (res.statusCode !== 200) {
      logger.error({statusCode: res.statusCode}, 'Received a non 200 response from Slack');
      return done(new Error('Non-200 response from Slack'));
    }
    if (!body.ok) {
      logger.error({body: body}, 'Error received from Slack');
      return done(new Error(body.error));
    }
    var channelId = body.channel.id;  
    self.channel.sendMessage('Welcome to Notifyre', channelId);
    self.channelId = channelId;
    logger.info({body: body}, 'Successfully opened channel for a user');
    return done(null, channelId);
  });
};

SlackClient.prototype.closeChannelForUser = function (token, channelId, timestamp, done) {
  var self = this;
  var options = {
    uri: '/chat.delete',
    qs: {
      token: token,
      ts: timestamp,
      channel: channelId,
      as_user: true
    }
  };
  self.request.get(options, function (err, res, body) {
    if (err) {
      logger.error({err: err}, 'Error closing channel for user');
      return done(err);
    }
    if (res.statusCode !== 200) {
      logger.error({statusCode: res.statusCode}, 'Received a non 200 response from Slack');
      return done(new Error('Non-200 response from Slack'));
    }
    if (!body.ok) {
      logger.error({body: body}, 'Error received from Slack');
      return done(new Error(body.error));
    }
    logger.info({body: body}, 'Successfully closed channel of a user');
    return done(null);    
  });
};

SlackClient.prototype.updateUserChat = function (options, done) {
  var self = this;
  var botToken = options.token;
  var userId = options.userId;
  self.getUserState(userId, botToken, function (err, presence) {
    if (err) {
      return done(err); 
    }
    if (presence != "active") {
      return done(new Error('User is not connected to any client'));
    }
    var reqOptions = {
      uri: '/chat.update',
      qs: {
        token: options.token,
        as_user: true,
        ts: options.timestamp,
        channel: options.channelId,
        attachments: JSON.stringify(options.attachments)
      }
    };
    self.request.get(reqOptions, function (err, res, body) {
      if (err) {
        logger.error({err: err}, 'Error updating user chat for a user');
        return done(err);
      }
      if (res.statusCode !== 200) {
        logger.error({statusCode: res.statusCode}, 'Received a non 200 response from Slack');
        return done(new Error('Non-200 response from Slack'));
      }
      if (!body.ok) {
        logger.error({body: body}, 'Error received from Slack');
        return done(new Error(body.error));
      }
      logger.info({body: body}, 'Successfully updated chat for a user');
      return done(null);       
    });
  });
};

SlackClient.prototype.postNotification = function (options, done) {
  var self = this;
  var botToken = options.token;
  var userId = options.userId;

  self.getUserState(userId, botToken, function (err, presence) {
    if (err) {
      return done(err);
    }
    if (presence != "active") {
      return done(new Error('User is not connected to any client'));
    }
    logger.info({userIsOnline: presence}, 'User state');
    var reqOptions = {
      uri: '/chat.postMessage',
      qs: {
        token: options.token,
        channel: options.channelId,
        attachments: JSON.stringify(options.attachments),
        as_user: true
      }
    };
    self.request.get(reqOptions, function (err, res, body) {
      if (err) {
        logger.error({err: err}, 'Error posting notification to a user');
        return done(err);
      }
      if (res.statusCode !== 200) {
        logger.error({statusCode: res.statusCode}, 'Received a non 200 response from Slack');
        return done(new Error('Non-200 response from Slack'));
      }
      if (!body.ok) {
        logger.error({body: body}, 'Error received from Slack');
        return done(new Error(body.error));
      }
      logger.info({body: body}, 'Successfully posted notification to a user');
      return done(null, body.ts); 
    });
  });
};

SlackClient.prototype.getUserState = function (userId, token, done) {
  var self = this;
  var options = {
    uri: '/users.getPresence',
    qs: {
      token: token,
      user: userId
    }
  };
  self.request.get(options, function (err, res, body) {
    if (err) {
      logger.error({err: err}, 'Error getting user state');
      return done(err);
    }
    if (res.statusCode !== 200) {
      logger.error({statusCode: res.statusCode}, 'Received a non 200 response from Slack');
      return done(new Error('Non-200 response from Slack'));
    }
    return done(null, body.presence);
  });
}

SlackClient.prototype.authUser = function (code, done) {
  var self = this;
  var options = {
    uri: '/oauth.access',
    qs: {
      client_id: self.slackId,
      client_secret: self.slackSecret,
      code: code,
      redirect_uri: "https://notifyre.now.sh/auth"
    }
  };
  self.request.get(options, function (err, res, body) {
    if (err) {
      logger.error({err: err}, 'Error adding bot to team /oauth.access');
      return done(err);
    }
    if (res.statusCode !== 200) {
      logger.error({statusCode: res.statusCode}, 'Received a non 200 response from Slack');
      return done(new Error('Non-200 response from Slack'));
    }
    if (!body.ok) {
      logger.error({body: body}, 'Error received from Slack');
      return done(new Error(body.error));
    }
    logger.info({body: body}, 'Successfully authenticated user');
    return done(null, body);    
  });
};

module.exports = SlackClient;
