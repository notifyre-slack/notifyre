var admin = require("firebase-admin");
// Fetch the service account key JSON file contents
var serviceAccount = require("../notifyre-backend-firebase-adminsdk-x4grg-9a338dcfdb.json")

admin.initializeApp({
  credential: admin.credential.cert("./notifyre-backend-firebase-adminsdk-x4grg-9a338dcfdb.json"),
  databaseURL: "https://notifyre-backend.firebaseio.com"
});

var db = admin.database();
var ref = db.ref();
var SlackClient = require('./slack-client');
var slackClient = new SlackClient();
var bunyan = require('bunyan');
var logger = bunyan.createLogger({
  name: 'lib/firebase-controller',
  serializers: bunyan.stdSerializers,
  src: true
});


exports.getTokenFromSlackAndPersist = function (req, res, next) {
	var code = req.query.code;
	slackClient.authUser(code, function (err, body) {
	    if (err) {
			logger.error({err: err},"Error authenticating and adding bot to team")
      		return res.status(500).send(err.message);
	    }

		var bot = body.bot;
	  	var tokenRef = ref.child("tokens");

	  	tokenRef.child(body.team_id).set({
  			team_name: body.team_name,
  			bot_user_id: bot.bot_user_id,
  			bot_access_token: bot.bot_access_token
	  	})
	  	.then(function() {
				var redirectUrl = 'https://' + body.team_name + '.slack.com';
				logger.info({
					redirectUrl: redirectUrl
				}, "Succesfully added team to firebase");
				slackClient.initRTMLChannel(bot.bot_access_token, function () {});
	    	return res.redirect(redirectUrl);
  		})
  		.catch(function(error) {
   			 logger.error({ err: error }, "Failed to add team to firebase");
   			 return res.status(500).send("Failed to add to team");
  		});;	  	
	});
};

exports.getTokenFromFirebase = function (team_id, done) {
 
	db.ref('/tokens/' + team_id).once('value').then(function(snapshot) {
  		var exists = (snapshot.val() !== null);
  		
  		if(!exists)
  			return done("Token not found", null);

  		var token = snapshot.val().bot_access_token;
  		logger.info("Returning bot token fetched from firebase");
  		return done(null, token);
	});
};