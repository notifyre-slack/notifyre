'usr strict';

var request = require('request');
var config = require('config');
var SlackClient = require('./slack-client');
var firebase = require('./firebase-controller');
var async = require('async');

var bunyan = require('bunyan');
var logger = bunyan.createLogger({
  name: 'lib/controllers',
  serializers: bunyan.stdSerializers,
  src: true
});

exports.postUserId = function (req, res, next) {
  logger.info({teamId:req.body.teamid, userId: req.body.userid, path: req.route.path}, 'Received post /userId request');
  firebase.getTokenFromFirebase(req.body.teamid, function (err, botToken) {

    if(err){
      return res.status(401).send(err);
    }

    logger.info({
      token: botToken
    }, 'Received token from firebase');
    var slackClient = new SlackClient();
    async.waterfall([
      function (cb) {
        slackClient.initRTMLChannel(botToken, cb);
      },
      function (cb) {
        slackClient.openChannelForUser(botToken,req.body.userid, function (err, channelId) {
          if (err) {
            logger.error({err: err}, 'Error opening channel for a user');
            return cb(err);
          }
          return cb(null, channelId);
        });
      }
    ], function (err, channelId) {
      if (err) {
        return res.status(500).send(err.message);
      }
      return res.status(200).json({
        dmid: channelId,
        token: botToken
      });
    })
  });
};

exports.postDeleteUser = function (req, res, next) {
  logger.info({dmId: req.body.dm_id}, 'Received post /delete request');
  var slackClient = new SlackClient();
  slackClient.closeChannelForUser(req.body.token,req.body.dm_id, req.body.ts, function (err) {
    if (err) {
      logger.error({err: err}, 'Error closing channel for a user');
      return res.status(500).send(err.message);
    }
    return res.sendStatus(200);
  });
};

exports.postUpdateUser = function (req, res, next) {
  logger.info({dmId: req.body.dm_id}, 'Received post /update request');
  var slackClient = new SlackClient();
  slackClient.updateUserChat({
    userId: req.body.user_id,
    token: req.body.token,
    channelId: req.body.dm_id,
    timestamp: req.body.ts,
    attachments: req.body.attachments
  }, function (err) {
    if (err) {
      logger.error({err: err}, 'Error updating notification for a user');
      return res.status(500).send(err.message);
    }
    return res.sendStatus(200);
  });
};

exports.postNotification = function (req, res, next) {
  logger.info({dmId: req.body.dm_id}, 'Received post /notify request');
  
  var slackClient = new SlackClient();
  slackClient.postNotification({
    userId: req.body.user_id,
    token: req.body.token,
    channelId: req.body.dm_id,
    message: req.body.message,
    attachments: req.body.attachments
  }, function (err, timestamp) {
    if (err) {
      logger.error({err: err}, 'Error posting notification to user');
      return res.status(500).send(err.message);
    }
    return res.status(200).json({
      dmid: req.body.dm_id,
      notificationid: req.body.notification_id,
      ts: timestamp
    });
  });
};

